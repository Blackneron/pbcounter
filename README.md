# PBcounter - README #
---

### Overview ###

The **PBcounter** scripts can manage different hit counters on different webpages.

### Screenshots ###

![PBcounter - Example page](development/readme/pbcounter1.png "PBcounter - Example page")

![PBcounter - Counter status page](development/readme/pbcounter2.png "PBcounter - Counter status page")

![PBcounter - Select counter page](development/readme/pbcounter3.png "PBcounter - Select counter page")

![PBcounter - Change counter page](development/readme/pbcounter4.png "PBcounter - Change counter page")

### Setup ###

* Upload the directory **pbcounter** to your webhost.
* Make sure the directory **pbcounter/txt** is writable.
* Check the example page **pbcounter/example.php** to see how to use the counter.
* Edit the counter configuration files in the folder **pbcounter/config**.
* Display the page **pbcounter/admin/admin.php** to manage the counters.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBcounter** scripts are licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
