<!-- ===================================================================
// HTML specification.
// ================================================================= -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


  <!-- =================================================================
  // HTML head - Begin.
  // =============================================================== -->
  <head>


    <!-- ===============================================================
    // Header title.
    // ============================================================= -->
    <title>
      PBcounter - The solution for counting your website visitors !
    </title>


    <!-- ===============================================================
    // Please specify the path to the css file of the pbcounter script.
    // The file is located in the pbcounter/css folder and will specify
    // how the counter look like. You can edit the css file to change
    // the colors, fonts etc. You only have to use the css styles if you
    // want to format the html output (type 1) of the counter module.
    // If you only use the plain text output (type 2 or type 3) you
    // don't need css styles and therefore you don't need to include
    // this file.
    // ============================================================= -->
    <link href="css/example.css" rel="stylesheet" type="text/css" />


  <!-- =================================================================
  // HTML head - End.
  // =============================================================== -->
  </head>


  <!-- =================================================================
  // HTML body - Begin.
  // =============================================================== -->
  <body>


    <!-- ===============================================================
    // DIV element to center the whole page - Begin.
    // ============================================================= -->
    <div class="center">


      <!-- =============================================================
      // Page title text.
      // =========================================================== -->
      <div class="title">
        PBcounter 1.2 - Example Page
      </div>


      <?php


        // =============================================================
        //
        //            E X A M P L E   C O U N T E R   P A G E
        //
        // =============================================================
        //
        // On this example page there are four different counters used.
        // Normally it makes no sense to use more than one counter on
        // the same page. Here we use them only for demonstration
        // purposes so that we can show the different types of counters
        // and how to include them into the html code.
        //
        // =============================================================

        // =============================================================
        // Please include the pbcounter script here. This module will
        // provide the functions necessary to activate and display
        // the counter. The pbcounter script has to be included only
        // once on every page which contains a counter.
        // =============================================================
        include("include/pbcounter.php");


        // =============================================================
        //        E X A M P L E   C O U N T E R   N U M B E R   1
        // =============================================================

        // =============================================================
        // Please specify the counter id. This is a unique number or
        // name for this counter. If your counter id is "12" you have
        // to create a configuration file named pbcounter_12.php which
        // holds all the configuration settings for this counter.
        // =============================================================
        $counter_id = 1;


        // =============================================================
        // Displays the counter id and some information on the example
        // page. This is not necessary and is used only to demonste the
        // different types of counters.
        // =============================================================
        echo "<div class=\"counter-id\">\n";
        echo "Big Example Counter / Counter-ID: 1<br />\n";
        echo "Counter type 1 with CSS style format / Left and right label used<br />\n";
        echo "</div>\n";


        // =============================================================
        // The next statement calls the pbcounter function to display
        // the counter. You can also call the pbcounter function without
        // using the variable and set the counter ID directly into the
        // braces like in the following example: pbcounter(1);
        // =============================================================
        pbcounter($counter_id);


        // =============================================================
        //        E X A M P L E   C O U N T E R   N U M B E R   2
        // =============================================================

        // Displays the counter information.
        echo "<div class=\"counter-id\">\n";
        echo "Small Example Counter / Counter-ID: 2<br />\n";
        echo "Counter type 1 with CSS style format / Only left label used<br />\n";
        echo "</div>\n";

        // Calls the pbcounter function.
        pbcounter(2);


        // =============================================================
        //        E X A M P L E   C O U N T E R   N U M B E R   3
        // =============================================================

        // Displays the counter information.
        echo "<div class=\"counter-id\">\n";
        echo "Simple Example Counter / Counter-ID: 3<br />\n";
        echo "Counter type 2 with plain text label and number<br />\n";
        echo "</div>\n";

        // Calls the pbcounter function.
        pbcounter(3);


        // =============================================================
        //        E X A M P L E   C O U N T E R   N U M B E R   4
        // =============================================================

        // Displays the counter information.
        echo "<div class=\"counter-id\">\n";
        echo "Very Simple Example Counter / Counter-ID: 4<br />\n";
        echo "Counter type 3 only with plain text number<br />\n";
        echo "</div>\n";

        // Calls the pbcounter function.
        pbcounter(4);


      ?>


      <!-- =============================================================
      // Copyright text.
      // =========================================================== -->
      <div class="copyright">
        Copyright &copy; 2010 - <a class="copyright" href="http://www.pb-soft.com" target="_blank">PB-Soft</a>
      </div>


    <!-- ===============================================================
    // DIV element to center the whole page - End.
    // ============================================================= -->
    </div>


  <!-- =================================================================
  // HTML body - End.
  // =============================================================== -->
  </body>


<!-- ===================================================================
// HTML page - End.
// ================================================================= -->
</html>
