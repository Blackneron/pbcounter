<?php


// =====================================================================
// Specify a name for this counter. The name will be displayed on the
// administration panel.
//
//   Example: "Frontpage"
//
// =====================================================================
$counter_name = "Forum";


// =====================================================================
// Specify if the counter should be displayed.
//
//   0: Counter display disabled
//
//   1: Counter display enabled
//
// =====================================================================
$counter_display = 1;


// =====================================================================
// Specify if the reset counter link should be displayed.
//
//   0: Reset link disabled
//
//   1: Reset link enabled
//
// =====================================================================
$reset_display = 1;


// =====================================================================
// Specify if the increase and decrease counter links should be
// displayed.
//
//   0: Change link disabled
//
//   1: Change link enabled
//
// =====================================================================
$change_display = 1;


// =====================================================================
// Specify if the option to change between different counters should
// be displayed.
//
//   0: Change option disabled
//
//   1: Change option enabled
//
// =====================================================================
$multi_display = 1;


// =====================================================================
// Specify the type of the counter display.
//
//   1: The counter text and number is shown with DIV and SPAN tags.
//
//   2: Only the counter text and number is shown.
//
//   3: Only the number is shown.
//
// =====================================================================
$counter_type = 1;


// =====================================================================
// Specify the counter text 1 which is used to display the counter.
// This text is placed in front of the the counter number.
//
//   Example 1: "We got "
//
//   Example 2: "You are visitor number: "
//
// =====================================================================
$counter_text1 = "Visits: ";


// =====================================================================
// Specify the counter text 2 which is used to display the counter.
// This text is placed after the counter number.
//
//   Example 1: " site visits this month !"
//
//   Example 2: " - Congratulation !: "
//
// =====================================================================
$counter_text2 = "";
